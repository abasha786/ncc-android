package com.agero.ncc.fragments;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeSignInFragment extends BaseFragment {

    @BindView(R.id.text_logo)
    TextView mTextLogo;

    @BindView(R.id.text_label_version)
    TextView mLableVersion;

    WelcomeActivity mWelcomeActivity;
    String invitationCode;
    public WelcomeSignInFragment() {
        // Intentionally empty
    }

    public static WelcomeSignInFragment newInstance(String invitationCode) {
        WelcomeSignInFragment fragment = new WelcomeSignInFragment();
        Bundle args = new Bundle();
        args.putString("invitationCode", invitationCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_welcome_signin, fragment_content, true);

        ButterKnife.bind(this, view);

        TokenManager.getInstance().signOut();
        mWelcomeActivity = (WelcomeActivity) getActivity();
        Toolbar toolbar = (Toolbar) mWelcomeActivity.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            invitationCode = bundle.getString("invitationCode");
        }
        setAppVersion();

        return superView;
    }
//    Bundle bundleEvent = new Bundle();
    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.LANDING;
    @OnClick(R.id.button_main_create_account)
    public void onViewClicked() {
        mWelcomeActivity.pushFragment(CreateAccountFragment.newInstance(invitationCode),getString(R.string.title_createaccount));
        eventAction = NccConstants.FirebaseEventAction.CREATE_ACCOUNT;
//        bundleEvent.putString(eventAction, eventCategory);
//                getMFirebaseAnalytics().logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        mWelcomeActivity.firebaseLogEvent(eventCategory,eventAction);
    }

    @OnClick(R.id.button_main_signin)
    public void onSignInClicked() {
        mWelcomeActivity.pushFragment(PasswordFragment.newInstance(invitationCode),getString(R.string.title_signin));
        eventAction = NccConstants.FirebaseEventAction.SIGN_IN;
        mWelcomeActivity.firebaseLogEvent(eventCategory,eventAction);
    }

    private void setAppVersion(){
        try {
            PackageInfo pInfo = mWelcomeActivity.getPackageManager().getPackageInfo(mWelcomeActivity.getPackageName(), 0);
            String version = pInfo.versionName;

            if("prod".equalsIgnoreCase(BuildConfig.ENV) ){
                mLableVersion.setText(version);
            } else{
                mLableVersion.setText(version + " - " + BuildConfig.ENV);
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

}
