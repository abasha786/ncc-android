package com.agero.ncc.fragments;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.HistoryListAdapter;
import com.agero.ncc.driver.fragments.JobDetailsFragment;
import com.agero.ncc.firestore.FirestoreJobData;
import com.agero.ncc.firestore.SearchFragment;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.RecyclerItemClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * To show Job History
 */
public class HistoryFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener{
//    @BindView(R.id.footer_history)
//    ConstraintLayout mConstraintFooter;

    @BindView(R.id.constraint_history)
    ConstraintLayout mConstraintHistory;
    @BindView(R.id.recycler_history)
    RecyclerView mRecyclerHistory;
    Unbinder unbinder;
    ArrayList<JobDetail> mHistroyList;
    HistoryListAdapter historyListAdapter;
    HomeActivity mHomeActivity;
    String mDispatchNumber;
    @BindView(R.id.text_recent_jobs)
    TextView mTextRecentJobs;
    @BindView(R.id.text_recent_jobs_desc)
    TextView mTextRecentJobsDesc;
    UserError mUserError;
    boolean isBottommBarVisible;
    StickyHeaderDecoration stickyHeaderDecoration;
    String selectedDispatchNumber;
    int selectedJobPosition;
    private ArrayList<String> headerDate;
    private long oldRetryTime = 0;
    private DatabaseReference myRef;
    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.HISTORY;
    private CompositeDisposable disposablesSorting = new CompositeDisposable();

    private ValueEventListener mHistoryValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (isAdded()) {
                hideProgress();
                mHistroyList.clear();
                if (dataSnapshot != null && dataSnapshot.hasChildren()) {
                    /*PerformanceImproverTask performanceImproverTask = new PerformanceImproverTask(dataSnapshot);
                    performanceImproverTask.execute();*/
                    performanceImproverTaskRx(dataSnapshot);
                }else{
                    mHistroyList = new ArrayList<>();
                    mHistroyList.clear();
                    loadData();
                }
            } else {
                hideProgress();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("History JobList Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    loadHistory();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private ArrayList<JobDetail> backUp;
    private ArrayList<String> headerBackup = new ArrayList<>();

    public HistoryFragment() {
        // Intentionally empty
    }

    public static HistoryFragment newInstance(boolean isBottomBarVisible, String mJobId) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(NccConstants.BUNDLE_KEY_IS_BOTTOM_VISIBLE, isBottomBarVisible);
        bundle.putString(NccConstants.JOB_ID,mJobId);
        fragment.setArguments(bundle);
        //fragment.selectedJobPosition = 0;
        return fragment;
    }

    private void doInSeparateThread(DataSnapshot dataSnapshot, ArrayList<JobDetail> mList) {

        String logEventText = "";
        Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
        for (DataSnapshot data : childList) {
            try {
                JobDetail job = data.getValue(JobDetail.class);
                String time = DateTimeUtils.getDisplayForDate(job.getDisablementLocation().getTimeZone(),DateTimeUtils.parse(job.getDispatchCreatedDate()));
                if (time != null) {
                    mList.add(job);
                    if (!logEventText.isEmpty()) {
                        logEventText += ", ";
                    }
                    logEventText += job.getDispatchId();
                }
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
            }
        }
        HashMap<String, String> extraDatas = new HashMap<>();
        extraDatas.put("jobs", logEventText);
        mHomeActivity.mintlogEventExtraData("Job History", extraDatas);
        Collections.sort(mList, (jobDetail, t1) -> {
            try {
               if(t1 != null && jobDetail != null) {
                   return DateTimeUtils.parse(t1.getDispatchCreatedDate()).compareTo(DateTimeUtils.parse(jobDetail.getDispatchCreatedDate()));
               } else{
                   return 0;
               }
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }

        });
        if (!mList.isEmpty()) {
            String time = null;
            try {
                time = DateTimeUtils.getDisplayForDate(mList.get(0).getDisablementLocation().getTimeZone(),DateTimeUtils.parse(mList.get(0).getDispatchCreatedDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            headerDate.add(time);
        }
        for (int i = 0; i < mList.size(); i++) {
            String time = null;
            try {
                if(mList.get(i) != null && mList.get(i).getDispatchCreatedDate() != null && mList.get(i).getDisablementLocation() != null) {
                    time = DateTimeUtils.getDisplayForDate(mList.get(i).getDisablementLocation().getTimeZone(),DateTimeUtils.parse(mList.get(i).getDispatchCreatedDate()));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (time != null && !headerDate.contains(time)) {
                headerDate.add(time);
            }
        }

        backUp = (ArrayList<JobDetail>) mList.clone();
        headerBackup = (ArrayList<String>) headerDate.clone();
    }

    private void loadData() {
        setAdapter();

//        mHomeActivity.enableSearchListener(this);
//        mHomeActivity.addOnDateSetListener(this);

        //if(!TextUtils.isEmpty(mHomeActivity.getFilterText())){
         //   onQueryTextSubmit(mHomeActivity.getFilterText());
        //}

        //if(mHomeActivity.getSelectedDate()!=null){
        //    onDateSet(mHomeActivity.getSelectedDate().getTime());
        //}

        if (mHistroyList != null && mHistroyList.size() > 0) {
            if (selectedDispatchNumber == null || TextUtils.isEmpty(selectedDispatchNumber)) {
                selectedDispatchNumber = mHistroyList.get(0).getDispatchId();
            }

            if (!TextUtils.isEmpty(selectedDispatchNumber) && getResources().getBoolean(R.bool.isTablet)) {
                if (isAdded()) {
                    FirestoreJobData.getInStance().setFirestore(false);
                    mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true));
                }
            }
            mTextRecentJobs.setVisibility(View.GONE);
            mTextRecentJobsDesc.setVisibility(View.GONE);

        } else {
            mTextRecentJobs.setVisibility(View.VISIBLE);
            mTextRecentJobsDesc.setVisibility(View.VISIBLE);
        }
        mRecyclerHistory.setVisibility(View.VISIBLE);
        mHomeActivity.submitSearch();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container_, @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container_, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_history, fragment_content, true);

        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        if (getArguments() != null && getArguments().getBoolean(NccConstants.BUNDLE_KEY_IS_BOTTOM_VISIBLE)) {
            mHomeActivity.showBottomBar();
        } else {
            mHomeActivity.hideBottomBar();
        }

        mHomeActivity.showToolbar(getString(R.string.title_history));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerHistory.setLayoutManager(linearLayoutManager);
        mRecyclerHistory.setItemAnimator(null);
        mRecyclerHistory.removeItemDecoration(new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation()));
        mHistroyList = new ArrayList<>();
        headerDate = new ArrayList<>();
        mUserError = new UserError();

        mTextRecentJobs.setVisibility(View.VISIBLE);
        mTextRecentJobsDesc.setVisibility(View.VISIBLE);
        mRecyclerHistory.setVisibility(View.GONE);
//        mConstraintFooter.setVisibility(View.GONE);

//        mConstraintFooter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mHomeActivity.push(SearchFragment.newInstance(true),getString(R.string.title_firestore_history));
//            }
//        });

       /* mRecyclerHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                mConstraintFooter.setVisibility(View.GONE);
                if (!recyclerView.canScrollVertically(1)) {
                    mConstraintFooter.setVisibility(View.VISIBLE);

                }
            }
        });*/

        mRecyclerHistory.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if(position < mHistroyList.size()) {
                    eventAction = NccConstants.FirebaseEventAction.VIEW_HISTORY_DETAIL;
                    mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
                    if (isAdded() && position != -1 && mHistroyList.size() > position) {
                        mDispatchNumber = mHistroyList.get(position).getDispatchId();
                        selectedDispatchNumber = mDispatchNumber;
                        selectedJobPosition = position;
                        FirestoreJobData.getInStance().setFirestore(false);

                        if (getResources().getBoolean(R.bool.isTablet)) {
                            mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true));
                        } else {
                            if (position >= 0) {
                                mHomeActivity.push(JobDetailsFragment.newInstance(mDispatchNumber, true), getString(R.string.title_assigned));
                            }
                        }
                    }
                }else{
                    mHomeActivity.push(SearchFragment.newInstance(true),getString(R.string.title_firestore_history));
                }
            }
        }));

        if(getArguments()!=null){
            selectedDispatchNumber = getArguments().getString(NccConstants.JOB_ID);
        }

        if (!getResources().getBoolean(R.bool.isTablet) && !TextUtils.isEmpty(selectedDispatchNumber)) {
            FirestoreJobData.getInStance().setFirestore(false);
            mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true), getString(R.string.title_jobdetail));
            getArguments().remove(NccConstants.JOB_ID);
            selectedDispatchNumber = null;
        }else{
            loadHistory();
        }

        return superView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        if(mHomeActivity!=null){
//            mHomeActivity.disableSearchListener();
//            mHomeActivity.clearDate();
//            mHomeActivity.clearSearch();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mHomeActivity.showToolbar(getString(R.string.title_history));
        mHomeActivity.setOnToolbarSaveListener(this);
    }

    private void loadHistory() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        if (mHomeActivity.isLoggedInDriver()) {
                            database.getReference("InActiveJobs/")
                                    .child(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))
                                    .orderByChild("dispatchAssignedToId")
                                    .equalTo(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))
                                    .addValueEventListener(mHistoryValueEventListener);
                        } else {
                            myRef = database.getReference("InActiveJobs/").child(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
                            //myRef.keepSynced(true);
                            myRef.addValueEventListener(mHistoryValueEventListener);

                            // Query  queryRef = myRef.orderByChild("vehicle/make").equalTo("Mercedes-Benz");
                            //Query  queryRef = myRef.orderByChild("vehicle/color").equalTo("Maroon");
                            // queryRef.addValueEventListener(mHistoryValueEventListener);

//                            Query  queryRef = myRef.orderByChild("dispatchCreatedDate").startAt("2018-03-14T00:00:00").endAt("2018-03-17T04:32:17");
//                            queryRef.addValueEventListener(mHistoryValueEventListener);
                        }
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    /*@Override
    public void onResume() {
        super.onResume();
        if (mHomeActivity != null) {
            mHomeActivity.enableSearchListener(null);
            mHomeActivity.clearSearch();
            if (mHistroyList != null) {
                mHistroyList.clear();
            }
            if (headerDate != null) {
                headerDate.clear();
            }
            if (backUp != null) {
                backUp.clear();
            }
            mHomeActivity.enableSearchListener(this);
        }
    }*/

    private void setAdapter() {

        historyListAdapter =
                new HistoryListAdapter(getActivity(), mHistroyList, headerDate, selectedJobPosition);

        if (mRecyclerHistory != null) {
            mRecyclerHistory.setAdapter(historyListAdapter);
            if (!mHistroyList.isEmpty()) {
                try {
                    if (stickyHeaderDecoration != null) {
                        mRecyclerHistory.removeItemDecoration(stickyHeaderDecoration);
                    }
                    stickyHeaderDecoration = new StickyHeaderDecoration(historyListAdapter);
                    mRecyclerHistory.addItemDecoration(stickyHeaderDecoration, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (unbinder != null) {
                unbinder.unbind();
            }
            if (myRef != null) {
                myRef.removeEventListener(mHistoryValueEventListener);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (myRef != null) {
                myRef.removeEventListener(mHistoryValueEventListener);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (myRef != null) {
                myRef.removeEventListener(mHistoryValueEventListener);
            }
            if(!disposablesSorting.isDisposed()){
                disposablesSorting.dispose();
            }
        } catch (Exception e) {

        }
    }

 /*   @Override
    public boolean onQueryTextSubmit(String query) {
        if(!TextUtils.isEmpty(query)){
            //mHomeActivity.clearDate();
            //onDateReset();

            *//*String headerDateString = null;
            if(mHomeActivity.getSelectedDate()!=null){
                Date date = mHomeActivity.getSelectedDate().getTime();
                headerDateString = DateTimeUtils.getDisplayForDate(date);
            }

            if (!TextUtils.isEmpty(query) && backUp != null && backUp.size() > 0) {
                ArrayList<JobDetail> filtered = new ArrayList<>();

                for (JobDetail job : backUp) {
                    if (job.getDispatchId() != null && job.getDispatchId().contains(query)) {
                        if(headerDateString!=null){
                            try {
                                Date jobDate = DateTimeUtils.parse(job.getDispatchCreatedDate());
                                String childdateString = DateTimeUtils.getDisplayForDate(jobDate);
                                if (childdateString.equals(headerDateString)) {
                                    filtered.add(job);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }else{
                            filtered.add(job);
                        }
                    }
                }

                if (mHistroyList != null) {
                    mHistroyList.clear();
                    mHistroyList.addAll(filtered);
                    historyListAdapter.notifyDataSetChanged();
                }

                if (mHistroyList == null || mHistroyList.size() == 0) {
                    noResultsAlertDialog();
                    mHomeActivity.hideRightFrame();
                } else {
                    if (mHistroyList != null && mHistroyList.size() > 0) {

                        selectedDispatchNumber = mHistroyList.get(0).getDispatchId();

                        if (!TextUtils.isEmpty(selectedDispatchNumber) && getResources().getBoolean(R.bool.isTablet)) {
                            if (isAdded()) {
//                            mHomeActivity.enableRightFrame();
                                mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true));
                            }
                        }
                    }
                }
            }*//*

            if(mHomeActivity.getSelectedDate()!=null){
                onDateSet(mHomeActivity.getSelectedDate().getTime());
            }else{
                onDateSet(null);
            }
        }


        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText) && backUp != null) {
            *//*mHistroyList.clear();
            mHistroyList.addAll(backUp);

            headerDate.clear();
            headerDate.addAll(headerBackup);
            historyListAdapter.notifyDataSetChanged();

            if (mHistroyList != null && mHistroyList.size() > 0) {
                selectedDispatchNumber = mHistroyList.get(0).getDispatchId();

                if (!TextUtils.isEmpty(selectedDispatchNumber) && getResources().getBoolean(R.bool.isTablet)) {
                    if (isAdded()) {
                        mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true));
                    }
                }
            }*//*
            if(mHomeActivity.getSelectedDate()!=null){
                onDateSet(mHomeActivity.getSelectedDate().getTime());
            }else{
                onReset();
            }


        }
        return false;
    }

    @Override
    public void onDateSet(Date date) {

        if(backUp == null){
            return;
        }

        headerDate.clear();

        String headerDateString = null;
        if(date!=null){
            headerDateString = DateTimeUtils.getDisplayForDate(date);
            headerDate.add(headerDateString);
        }

        mHistroyList.clear();

        String filterText = mHomeActivity.getFilterText();

        if(TextUtils.isEmpty(headerDateString) && TextUtils.isEmpty(filterText)){
            onReset();
        }else{
            for (JobDetail jobDetail : backUp) {
                //both filter applied
                if(!TextUtils.isEmpty(headerDateString) && !TextUtils.isEmpty(filterText)){
                    try {
                        Date jobDate = DateTimeUtils.parse(jobDetail.getDispatchCreatedDate());
                        String childdateString = DateTimeUtils.getDisplayForDate(jobDetail.getDisablementLocation().getTimeZone(),jobDate);
                        if (childdateString.equals(headerDateString)) {
                            if (jobDetail.getDispatchId() != null && jobDetail.getDispatchId().contains(filterText)) {
                                mHistroyList.add(jobDetail);
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                //any one filter applied
                else{
                    //date filter applied
                    if(!TextUtils.isEmpty(headerDateString)){
                        try {
                            Date jobDate = DateTimeUtils.parse(jobDetail.getDispatchCreatedDate());
                            String childdateString = DateTimeUtils.getDisplayForDate(jobDetail.getDisablementLocation().getTimeZone(),jobDate);
                            if (childdateString.equals(headerDateString)) {
                                mHistroyList.add(jobDetail);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    //id filter applied
                    else{
                        if (jobDetail.getDispatchId() != null && jobDetail.getDispatchId().contains(filterText)) {
                            mHistroyList.add(jobDetail);
                            try {
                                Date jobDate = DateTimeUtils.parse(jobDetail.getDispatchCreatedDate());
                                String childdateString = DateTimeUtils.getDisplayForDate(jobDetail.getDisablementLocation().getTimeZone(),jobDate);
                                if(!headerDate.contains(childdateString)){
                                    headerDate.add(childdateString);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }

        *//*for (JobDetail jobDetail : backUp) {
            try {
                Date jobDate = DateTimeUtils.parse(jobDetail.getDispatchCreatedDate());
                String childdateString = DateTimeUtils.getDisplayForDate(jobDate);
                if (childdateString.equals(headerDateString)) {
                    if(!TextUtils.isEmpty(filterText)){
                        if (jobDetail.getDispatchId() != null && jobDetail.getDispatchId().contains(filterText)) {
                            mHistroyList.add(jobDetail);
                        }
                    }else{
                        mHistroyList.add(jobDetail);
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }*//*

        if(historyListAdapter != null) {
            historyListAdapter.notifyDataSetChanged();
        }
        selectedJobPosition = 0;
        if (mHistroyList == null || mHistroyList.size() == 0) {
            noResultsAlertDialog();
            mHomeActivity.hideRightFrame();
        } else {
            if (mHistroyList != null && mHistroyList.size() > 0) {

                selectedDispatchNumber = mHistroyList.get(0).getDispatchId();

                if (!TextUtils.isEmpty(selectedDispatchNumber) && getResources().getBoolean(R.bool.isTablet)) {
                    if (isAdded()) {
//                        mHomeActivity.enableRightFrame();
                        mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true));
                    }
                }
            }
        }
        setAdapter();
    }

    @Override
    public void onReset() {
        headerDate.clear();
        if(headerBackup != null) {
            headerDate.addAll(headerBackup);
        }

        mHistroyList.clear();
        if(backUp != null) {
            mHistroyList.addAll(backUp);
        }

        setAdapter();

        if (mHistroyList != null && mHistroyList.size() > 0) {
            selectedDispatchNumber = mHistroyList.get(0).getDispatchId();

            if (!TextUtils.isEmpty(selectedDispatchNumber) && getResources().getBoolean(R.bool.isTablet)) {
                if (isAdded()) {
                    mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true));
                }
            }
        }


    }*/

    private void noResultsAlertDialog() {
        if (isAdded()) {
            String msg = getResources().getString(R.string.search_no_jobs_history);
            AlertDialogFragment alert = AlertDialogFragment.newDialog(getString(R.string.search_no_results), Html.fromHtml("<big>" + msg + "</big>")
                    , Html.fromHtml("<b>" + getString(R.string.search_try_again) + "</b>"), null);

            alert.setListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (isAdded()) {
                        selectedJobPosition = 0;
//                        mHomeActivity.clearSearch();
//                        mHomeActivity.clearDate();
//                        onReset();
                        dialogInterface.dismiss();
                    }
                }
            });

            if (getFragmentManager() != null) {
                try {
                    alert.show(getFragmentManager().beginTransaction(), AccountFragment.class.getClass().getCanonicalName());
                } catch (Exception e) {
                }
            }
        }
    }

    private void performanceImproverTaskRx(DataSnapshot dataSnapshot){
        showProgress();
        ArrayList<JobDetail> mList = (ArrayList<JobDetail>) mHistroyList.clone();
        disposablesSorting.add(Observable.fromCallable(() -> {
            doInSeparateThread(dataSnapshot,mList);
            return false;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    hideProgress();
                    mHistroyList = mList;
                    if (isAdded()) {
                        loadData();
                    }
                },Throwable::printStackTrace));
    }

    @Override
    public void onSave() {
        mHomeActivity.push(SearchFragment.newInstance(false),getString(R.string.title_firestore_history));
    }

    /*class PerformanceImproverTask extends AsyncTask<Void, Void, Void> {
        private DataSnapshot mDataSnapshot;
        private ArrayList<JobDetail> mList;

        PerformanceImproverTask(DataSnapshot dataSnapshot) {
            mDataSnapshot = dataSnapshot;
            mList = (ArrayList<JobDetail>)mHistroyList.clone();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        @Override
        protected Void doInBackground(Void... params) {
            doInSeparateThread(mDataSnapshot,mList);
            return null;
        }

        protected void onPostExecute(Void bitmap) {
            hideProgress();
            mHistroyList = mList;
            if (isAdded()) {
                loadData();
            }
        }
    }*/

}
