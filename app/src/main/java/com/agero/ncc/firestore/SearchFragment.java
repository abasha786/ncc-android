package com.agero.ncc.firestore;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.HistorySearchAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.driver.fragments.JobDetailsFragment;
import com.agero.ncc.fragments.BaseFragment;
import com.agero.ncc.fragments.DateListener;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.RecyclerItemClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SearchFragment extends BaseFragment implements SearchView.OnQueryTextListener, DateListener, HomeActivity.ToolbarSaveListener {
    HomeActivity mHomeActivity;
    @BindView(R.id.recycler_firestore_history)
    RecyclerView mRecyclerHistory;
    ArrayList<JobDetail> mHistroyList;
    ArrayList<JobDetail> mFirestoreHistroyList;
    ArrayList<JobDetail> mFirebaseHistroyList;
    private boolean mIsFirestore = true;
    private String mSearchId = "";
    private FirebaseFirestore mFirestore;
    private ListenerRegistration mRegistration;
    private long oldRetryTime = 0;
    private DatabaseReference myRef;
    private int selectedJobPosition = 0;
    private String mSearchDate = "";
    private String selectedDispatchNumber = "";
    private CompositeDisposable disposablesSorting = new CompositeDisposable();

    @BindView(R.id.footer_history_constraint)
    ConstraintLayout mConstraintFooter;
    @BindView(R.id.text_job_search_rule)
    TextView mFooterTv;
    String eventCategory = NccConstants.FirebaseEventCategory.HISTORY_SEARCH;


    private EventListener eventListner = new EventListener<QuerySnapshot>() {
        @Override
        public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {

            if (isAdded()) {
                if (mRegistration != null) {
                    mRegistration.remove();
                    mRegistration = null;
                }
                hideProgress();
                mFooterTv.setText(getString(R.string.text_jobs_history_rule_less));
                mFirestoreHistroyList.clear();
                ArrayList<JobDetail> jobDetails = new ArrayList<>();
                if (queryDocumentSnapshots != null) {
                    mConstraintFooter.setVisibility(View.GONE);
                    mRecyclerHistory.setVisibility(View.VISIBLE);
                    for (DocumentSnapshot change : queryDocumentSnapshots.getDocuments()) {
                        JobDetail jobDetail = change.toObject(JobDetail.class);
                        jobDetails.add(jobDetail);
                    }
                    if (jobDetails.size() > 0) {
//                        PerformanceImproverTask performanceImproverTask = new PerformanceImproverTask(jobDetails);
//                        performanceImproverTask.execute();
                        performanceImproverTaskRx(jobDetails);
                    } else {
                        mConstraintFooter.setVisibility(View.VISIBLE);
                        mRecyclerHistory.setVisibility(View.GONE);
                        loadData();
                    }
                } else {
                    mConstraintFooter.setVisibility(View.VISIBLE);
                    mRecyclerHistory.setVisibility(View.GONE);
                    loadData();
                }
            } else {
                hideProgress();
            }
        }
    };

    private ValueEventListener mHistoryValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (isAdded()) {
                try {
                    if (myRef != null) {
                        myRef.removeEventListener(mHistoryValueEventListener);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hideProgress();
                mFirebaseHistroyList.clear();
                mFooterTv.setText(getString(R.string.text_jobs_history_rule));
                ArrayList<JobDetail> jobDetails = new ArrayList<>();
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot data :
                            dataSnapshot.getChildren()) {
                        try {
                            JobDetail job = data.getValue(JobDetail.class);
                            jobDetails.add(job);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (jobDetails.size() > 0) {
                        mConstraintFooter.setVisibility(View.GONE);
                        mRecyclerHistory.setVisibility(View.VISIBLE);
                      /*  PerformanceImproverTask performanceImproverTask = new PerformanceImproverTask(jobDetails);
                        performanceImproverTask.execute();*/
                        performanceImproverTaskRx(jobDetails);
                    } else {
                        mConstraintFooter.setVisibility(View.VISIBLE);
                        mRecyclerHistory.setVisibility(View.GONE);
                        mHomeActivity.hideRightFrame();
                        mHomeActivity.showToolbar(getString(R.string.title_firestore_history));
                        mHomeActivity.setOnToolbarSaveListener(SearchFragment.this);
                    }
                } else {
                    mConstraintFooter.setVisibility(View.VISIBLE);
                    mRecyclerHistory.setVisibility(View.GONE);
                    mHomeActivity.hideRightFrame();
                    mHomeActivity.showToolbar(getString(R.string.title_firestore_history));
                    mHomeActivity.setOnToolbarSaveListener(SearchFragment.this);
                }
            } else {
                hideProgress();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("History JobList Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    loadSearch();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    public SearchFragment() {
        // Intentionally empty
    }

    public static SearchFragment newInstance(boolean mIsFirestore) {
        SearchFragment fragment = new SearchFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("IS_FIRESTORE", mIsFirestore);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.history_search, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mHomeActivity = (HomeActivity) getActivity();
        if (getResources().getBoolean(R.bool.isTablet)) {
            mHomeActivity.showBottomBar();
        } else {
            mHomeActivity.hideBottomBar();
        }
        if (getArguments() != null) {
            mIsFirestore = getArguments().getBoolean("IS_FIRESTORE");
        }
        mHomeActivity.showToolbar(getString(R.string.title_firestore_history));
        mHomeActivity.setOnToolbarSaveListener(this);

        mHistroyList = new ArrayList<>();
        mFirebaseHistroyList = new ArrayList<>();
        mFirestoreHistroyList = new ArrayList<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerHistory.setLayoutManager(linearLayoutManager);
        mConstraintFooter.setVisibility(View.GONE);
        mRecyclerHistory.setVisibility(View.GONE);

        mRecyclerHistory.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String eventAction = NccConstants.FirebaseEventAction.VIEW_HISTORY_DETAIL;
                mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
                if (isAdded() && position != -1 && mHistroyList.size() > position) {
                    String mDispatchNumber = mHistroyList.get(position).getDispatchId();
                    selectedDispatchNumber = mDispatchNumber;
                    selectedJobPosition = position;
                    FirestoreJobData.getInStance().setJobDetail(mHistroyList.get(position));
                    FirestoreJobData.getInStance().setFirestore(mIsFirestore);
                    if (mHomeActivity.isLoggedInDriver()) {
                        if (getResources().getBoolean(R.bool.isTablet)) {
                            mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true, true));
                        } else {
                            if (position >= 0) {
                                mHomeActivity.push(JobDetailsFragment.newInstance(mDispatchNumber, true), getString(R.string.title_assigned));
                            }
                        }
                    } else {
                        if (getResources().getBoolean(R.bool.isTablet)) {
                            mHomeActivity.push(com.agero.ncc.dispatcher.fragments.JobDetailsFragment.newInstance(selectedDispatchNumber, true, true));
                        } else {
                            if (position >= 0) {
                                mHomeActivity.push(com.agero.ncc.dispatcher.fragments.JobDetailsFragment.newInstance(mDispatchNumber, true), getString(R.string.title_assigned));
                            }
                        }
                    }
                }
            }
        }));

        mConstraintFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsFirestore = !mIsFirestore;
                mConstraintFooter.setVisibility(View.GONE);
                mRecyclerHistory.setVisibility(View.GONE);
            }
        });

        return superView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mHomeActivity != null) {
            mHomeActivity.disableSearchListener();
            mHomeActivity.clearDate();
            mHomeActivity.clearSearch();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mHomeActivity != null) {
            mHomeActivity.enableSearchListener(this);
            mHomeActivity.addOnDateSetListener(this);
        }
    }

    private void loadSearch() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        if (mIsFirestore) {
                            mFirestore = FirebaseFirestore.getInstance();
                            Query mQuery = mFirestore.collection("InActiveJobs").whereEqualTo("facilityId", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));

                            if (!TextUtils.isEmpty(mSearchId)) {
                                if (mSearchId.length() == 12) {
                                    mQuery = mQuery.whereEqualTo("dispatchId", mSearchId);
                                } else {
                                    mQuery = mQuery.orderBy("dispatchId").startAt(addPadding(mSearchId, true)).endAt(addPadding(mSearchId, false));

                                }
                            }
                            if (!TextUtils.isEmpty(mSearchDate)) {
                                mQuery = mQuery.whereEqualTo("dispatchDate", mSearchDate);
                            }


                            if (mHomeActivity.isLoggedInDriver()) {
                                mQuery = mQuery.whereEqualTo("dispatchAssignedToId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                            }

                            mRegistration = mQuery.addSnapshotListener(eventListner);

                        } else {
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            if (mHomeActivity.isLoggedInDriver()) {
                                database.getReference("InActiveJobs/")
                                        .child(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))
                                        .orderByChild("dispatchAssignedToId")
                                        .equalTo(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))
                                        .addValueEventListener(mHistoryValueEventListener);
                            } else {
                                myRef = database.getReference("InActiveJobs/").child(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
                                myRef.addValueEventListener(mHistoryValueEventListener);
                            }
                        }
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            UserError mUserError = new UserError();
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private String addPadding(String mSearchId, boolean isStartAt) {
        int padLength = 12 - mSearchId.length();
        String padDigit = "9";
        if (isStartAt) {
            padDigit = "0";
        }

        for (int i = 0; i < padLength; i++) {
            mSearchId += padDigit;
        }
        return mSearchId;
    }

    @Override
    public void onSave() {
        mHomeActivity.setSearchText(mSearchId, mSearchDate);
        if (TextUtils.isEmpty(mSearchDate) && TextUtils.isEmpty(mSearchId)) {
            mHistroyList.clear();
            loadData();
            return;
        }

        if (mIsFirestore) {
            loadSearch();
        } else if (mFirebaseHistroyList == null || mFirebaseHistroyList.size() == 0) {
            loadSearch();
        } else {
            localSearch();
        }
    }

    private void localSearch() {
        if (isAdded()) {
            mHistroyList.clear();
            for (JobDetail jobDetail : mFirebaseHistroyList) {
                boolean isSearchMatched = true;
                if (!TextUtils.isEmpty(mSearchId) && !(jobDetail.getDispatchId().contains(mSearchId))) {
                    isSearchMatched = false;
                }
                if (!TextUtils.isEmpty(mSearchDate) && !(jobDetail.getDispatchDate().equalsIgnoreCase(mSearchDate))) {
                    isSearchMatched = false;
                }
                if (isSearchMatched) {
                    mHistroyList.add(jobDetail);
                }
            }
            if (mHistroyList.size() == 0) {
                mConstraintFooter.setVisibility(View.VISIBLE);
                mFooterTv.setText(getString(R.string.text_jobs_history_rule));
                mRecyclerHistory.setVisibility(View.GONE);
                mHomeActivity.hideRightFrame();
                mHomeActivity.showToolbar(getString(R.string.title_firestore_history));
                mHomeActivity.setOnToolbarSaveListener(this);
            } else {
                mConstraintFooter.setVisibility(View.GONE);
                mRecyclerHistory.setVisibility(View.VISIBLE);
                loadData();
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mSearchId = query.replace("-", "");

        if (TextUtils.isEmpty(mSearchDate) && TextUtils.isEmpty(mSearchId)) {
            mHistroyList.clear();
            loadData();
            return false;
        }
        if (mIsFirestore) {
            loadSearch();
        } else if (mFirebaseHistroyList == null || mFirebaseHistroyList.size() == 0) {
            loadSearch();
        } else {
            localSearch();
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mSearchId = newText.replace("-", "");
        return false;
    }

    @Override
    public void onDateSet(Date date) {
        if (date != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            mSearchDate = simpleDateFormat.format(date);
        } else {
            mSearchDate = null;
        }
        onSave();
    }

    @Override
    public void onReset() {
    }

    private void performanceImproverTaskRx(ArrayList<JobDetail> dataSnapshot) {
        showProgress();
        ArrayList<JobDetail> mList = new ArrayList<>();
        disposablesSorting.add(Observable.fromCallable(() -> {
            doInSeparateThread(dataSnapshot, mList);
            return false;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    hideProgress();
                    if (mIsFirestore) {
                        mFirestoreHistroyList = mList;
                    } else {
                        mFirebaseHistroyList = mList;
                    }
                    if (isAdded()) {
                        mHistroyList = (ArrayList<JobDetail>) mList.clone();
                        if (mIsFirestore) {
                            loadData();
                        } else {
                            localSearch();
                        }
                    }
                }));
    }
  /*  class PerformanceImproverTask extends AsyncTask<Void, Void, Void> {
        private ArrayList<JobDetail> mDataSnapshot;
        private ArrayList<JobDetail> mList;

        PerformanceImproverTask(ArrayList<JobDetail> dataSnapshot) {
            mDataSnapshot = dataSnapshot;
            mList = new ArrayList<>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        @Override
        protected Void doInBackground(Void... params) {
            doInSeparateThread(mDataSnapshot, mList);
            return null;
        }

        protected void onPostExecute(Void bitmap) {
            hideProgress();
            if (mIsFirestore) {
                mFirestoreHistroyList = mList;
            } else {
                mFirebaseHistroyList = mList;
            }
            if (isAdded()) {
                mHistroyList = (ArrayList<JobDetail>) mList.clone();
                if(mIsFirestore) {
                    loadData();
                }else{
                    localSearch();
                }
            }
        }
    }*/

    private void loadData() {
        if (isAdded()) {
            HistorySearchAdapter historyListAdapter =
                    new HistorySearchAdapter(getActivity(), mHistroyList, selectedJobPosition);

            if (mRecyclerHistory != null) {
                mRecyclerHistory.setAdapter(historyListAdapter);
            }
            if (getResources().getBoolean(R.bool.isTablet) && mHistroyList.size() > 0) {
                String mDispatchNumber = mHistroyList.get(0).getDispatchId();
                selectedDispatchNumber = mDispatchNumber;
                selectedJobPosition = 0;
                FirestoreJobData.getInStance().setJobDetail(mHistroyList.get(0));
                FirestoreJobData.getInStance().setFirestore(mIsFirestore);

                if (mHomeActivity.isLoggedInDriver()) {
                    mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, true, true));
                } else {
                    mHomeActivity.push(com.agero.ncc.dispatcher.fragments.JobDetailsFragment.newInstance(selectedDispatchNumber, true, true));
                }
            } else if (mHistroyList.size() == 0) {
                mHomeActivity.hideRightFrame();
                mHomeActivity.showToolbar(getString(R.string.title_firestore_history));
                mHomeActivity.setOnToolbarSaveListener(this);
            }

        }
    }

    private void doInSeparateThread(ArrayList<JobDetail> dataSnapshot, ArrayList<JobDetail> mList) {

        String logEventText = "";

        for (JobDetail job : dataSnapshot) {
            try {
                String time = DateTimeUtils.getDisplayForDate(job.getDisablementLocation().getTimeZone(), DateTimeUtils.parse(job.getDispatchCreatedDate()));
                if (time != null) {
                    mList.add(job);
                    if (!logEventText.isEmpty()) {
                        logEventText += ", ";
                    }
                    logEventText += job.getDispatchId();
                }
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
            }
        }
        HashMap<String, String> extraDatas = new HashMap<>();
        extraDatas.put("jobs", logEventText);
        mHomeActivity.mintlogEventExtraData("Job Search History", extraDatas);
        Collections.sort(mList, (jobDetail, t1) -> {
            try {
                if (t1 != null && jobDetail != null) {
                    return DateTimeUtils.parse(t1.getDispatchCreatedDate()).compareTo(DateTimeUtils.parse(jobDetail.getDispatchCreatedDate()));
                } else {
                    return 0;
                }
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }

        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mHomeActivity.hideRightFrame();
            if (myRef != null) {
                myRef.removeEventListener(mHistoryValueEventListener);
            }
            if (!disposablesSorting.isDisposed()) {
                disposablesSorting.dispose();
            }
            if (mRegistration != null) {
                mRegistration.remove();
                mRegistration = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
