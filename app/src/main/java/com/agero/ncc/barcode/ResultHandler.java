package com.agero.ncc.barcode;

import com.google.zxing.Result;

public interface ResultHandler {
    void handleResult(Result rawResult);
}
