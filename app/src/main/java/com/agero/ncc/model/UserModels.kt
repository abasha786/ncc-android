package com.agero.ncc.model

/**
 * Created by sdevabhaktuni on 12/4/17.
 */

data class VerifyUser(
        var userExists: Boolean
       )

data class SigninResponseModel(
        var id_token: String?,
        var refresh_token: String?,
        var firebase_token: String?,
        var expires_in: Long,
        var user_profile: user_profile,
        var user_claims: user_claims
        )

data class user_profile(
        var firstName: String,
        var lastName: String,
        var email: String,
        var mobilePhoneNumber: String

        )

data class user_claims(
        var user_id: String,
        var facility_id: String,
        var app_name: String,
        var roles: ArrayList<String>
      )

data class SigninRequest(var username: String,var password: String, var deviceId: String)


data class CreateUserRequest(
        var firstName: String,
        var lastName: String,
        var mobilePhoneNumber: String,
        var password: String,
        var email: String,
        var invitationCode: String?=null,
        var deviceId: String
)

data class ForgotPasswordRequest(var email: String,var deviceId: String)


data class TokenModel(var platform:String, var pushToken:String, var time:String)
data class AuthTokenModel(var platform:String, var authtoken:String, var time:String,var lastRefreshedTime:String)

data class RefreshTokenRequest(
        var refresh_token: String,
        var deviceId: String
)

data class TermsAndConditionsModel(var content:String, var version:String)

data class TermsAndConditionsAcceptenceModel(var version:String)


data class CreateUserResponse(
        var user: User,
        var credentials: Credentials
)

data class User(
        var firstName: String,
        var lastName: String,
        var email: String,
        var mobilePhoneNumber: String,
        var userId: String,
        var facility_id: String,
        var app_name: String,
        var roles: ArrayList<String>,
        var status: String,
        var isOwner: String,
        var created_by: String,
        var created_at: String,
        var backgroundCheckStatus: String
)

data class Credentials(
        var id_token: String,
        var refresh_token: String,
        var firebase_token: String,
        var expires_in: Long,
        var user_profile: user_profile,
        var user_claims: user_claims
)

data class SparkLocationData(
        var timestamp: Long,
        var latitude: Double,
        var longitude: Double,
        var altitude: Double,
        var course: Float,
        var accuracy: Float,
        var speed: Float,
        var distance: Double,
        var os: String,
        var deviceId: String)
