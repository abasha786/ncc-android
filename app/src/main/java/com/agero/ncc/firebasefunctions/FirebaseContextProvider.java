package com.agero.ncc.firebasefunctions;

import android.support.annotation.NonNull;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.internal.api.FirebaseNoSignedInUserException;

class FirebaseContextProvider implements ContextProvider {
    private FirebaseApp app;

    FirebaseContextProvider(FirebaseApp app) {
        this.app = app;
    }

    public Task<HttpsCallableContext> getContext() {
        final FirebaseInstanceId firebaseInstanceId = FirebaseInstanceId.getInstance(this.app);
        return this.app.getToken(false).continueWith(new Continuation<GetTokenResult, HttpsCallableContext>() {
            public HttpsCallableContext then(@NonNull Task<GetTokenResult> task) throws Exception {
                String authToken = null;
                if (!task.isSuccessful()) {
                    Exception exception = task.getException();
                    if (!(exception instanceof FirebaseApiNotAvailableException) && !(exception instanceof FirebaseNoSignedInUserException)) {
                        throw exception;
                    }
                } else {
                    authToken = ((GetTokenResult)task.getResult()).getToken();
                }

                String instanceIdToken = firebaseInstanceId.getToken();
                HttpsCallableContext context = new HttpsCallableContext(authToken, instanceIdToken);
                return context;
            }
        });
    }
}

