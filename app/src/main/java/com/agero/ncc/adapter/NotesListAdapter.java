package com.agero.ncc.adapter;


import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.model.Comment;
import com.agero.ncc.utils.DateTimeUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotesListAdapter extends RecyclerView.Adapter<NotesListAdapter.ItemViewHolder> {


    private Context mContext;
    private ArrayList<Comment> mNotesList;
    private String mTimeZone;

    public NotesListAdapter(Context mContext, ArrayList<Comment> mNotesList,String timeZone) {
        this.mContext = mContext;
        this.mNotesList = mNotesList;
        this.mTimeZone = timeZone;
        if (TextUtils.isEmpty(timeZone)) {
            mTimeZone = Calendar.getInstance().getTimeZone().getID();
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.notes_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Comment notes = mNotesList.get(holder.getAdapterPosition());
        String text = "";
        GradientDrawable drawableNotes = (GradientDrawable) holder.mNotesLayout.getBackground();
        if(notes.getServerTimeUtc()!=null && !notes.getServerTimeUtc().isEmpty()){
            String time = notes.getServerTimeUtc();
            Date date = null;
            try {
                date = DateTimeUtils.parse(time);
                DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
                DateFormat timeFormat = new SimpleDateFormat("hh:mm a zzz",Locale.US);



                dateFormat.setTimeZone(TimeZone.getTimeZone(mTimeZone));

                timeFormat.setTimeZone(TimeZone.getTimeZone(mTimeZone));

                text = " " + mContext.getString(R.string.dot) + " " +dateFormat.format(date) + " " + mContext.getString(R.string.dot) + " " + timeFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if(mContext.getString(R.string.note_text_agero).equalsIgnoreCase(notes.getUserName())){
            holder.mTextNotesTime.setText(notes.getUserName() + text);
//            holder.mNotesLayout.setBackgroundColor(mContext.getResources()
//                    .getColor(R.color.onboarding_background_color));
            drawableNotes.setColor(mContext.getResources().getColor(R.color.onboarding_background_color));
        }else {
            drawableNotes.setColor(mContext.getResources().getColor(R.color.ncc_background));
            holder.mTextNotesTime.setText(notes.getUserName() + text);
//            holder.mNotesLayout.setBackgroundColor(mContext.getResources()
//                    .getColor(R.color.ncc_background));

        }
        holder.mTextNotesMessage.setText(notes.getCommentText());
    }

    @Override
    public int getItemCount() {
        return mNotesList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_notes_time)
        TextView mTextNotesTime;
        @BindView(R.id.text_notes_message)
        TextView mTextNotesMessage;
        @BindView(R.id.text_notes_layout)
        ConstraintLayout mNotesLayout;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
