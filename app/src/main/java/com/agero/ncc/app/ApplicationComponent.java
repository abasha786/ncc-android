package com.agero.ncc.app;


import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.ChatActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.adapter.AssignDriverListAdapter;
import com.agero.ncc.db.viewmodel.AlertListViewModel;
import com.agero.ncc.fragments.AccountFragment;
import com.agero.ncc.driver.fragments.ActiveJobsFragment;
import com.agero.ncc.fragments.AddVehicleDetailsFragment;
import com.agero.ncc.fragments.AlertDetailFragment;
import com.agero.ncc.fragments.AlertFragment;
import com.agero.ncc.fragments.BaseFragment;
import com.agero.ncc.fragments.BaseJobDetailsFragment;
import com.agero.ncc.fragments.BasicDetailsFragment;
import com.agero.ncc.fragments.CreateAccountFragment;
import com.agero.ncc.fragments.EditProfileFragment;
import com.agero.ncc.fragments.EquipmentSelectionFragment;
import com.agero.ncc.fragments.FingerprintAuthenticationDialogFragment;
import com.agero.ncc.fragments.LegalFragment;
import com.agero.ncc.fragments.MobileSignInFragment;
import com.agero.ncc.fragments.PasswordFragment;
import com.agero.ncc.fragments.ScanVinBarCodeFragment;
import com.agero.ncc.fragments.TermsAndConditionsFragment;
import com.agero.ncc.fragments.TowDestinationFragment;
import com.agero.ncc.retrofit.RetrofitModule;
import com.agero.ncc.services.NccFirebaseMessagingService;
import com.agero.ncc.utils.TokenManager;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by sdevabhaktuni on 9/19/16.
 */

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class,RoomModule.class})
public interface ApplicationComponent {

    void inject(NCCApplication application);

    void inject(BaseActivity baseActivity);

    void inject(HomeActivity homeActivity);

    void inject(WelcomeActivity welcomeActivity);

    void inject(BaseFragment baseFragment);

    void inject(AccountFragment accountFragment);

    void inject(ActiveJobsFragment activeJobsFragment);

    void inject(LegalFragment legalFragment);

    void inject(BasicDetailsFragment editAccountFragment);

    void inject(ScanVinBarCodeFragment scanVinBarCodeFragment);

    void inject(EquipmentSelectionFragment equipmentSelectionFragment);

    void inject(MobileSignInFragment mobileSignInFragment);

    void inject(AddVehicleDetailsFragment addVehicleDetailsFragment);

    void inject(NccFirebaseMessagingService nccFirebaseMessagingService);

    void inject(AlertFragment alertFragment);

    void inject(AlertDetailFragment alertDetailFragment);

    void inject(AlertListViewModel alertListViewModel);

    void inject(PasswordFragment passwordFragment);

    void inject(CreateAccountFragment createAccountFragment);

    void inject(AssignDriverListAdapter assignDriverListAdapter);

    void inject(TermsAndConditionsFragment termsAndConditionsFragment);

    void inject(TokenManager tokenManager);

    void inject(EditProfileFragment editProfileFragment);

    void inject(BaseJobDetailsFragment baseJobDetailsFragment);

    void inject(TowDestinationFragment towDestinationFragment);

    void inject(ChatActivity chatActivity);

    void inject(FingerprintAuthenticationDialogFragment fingerprintAuthenticationDialogFragment);
}
